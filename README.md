Ansible SSL Cert Docker
=========

Ansible role to generate a minimalistic SSL key/certificate pair _locally_ by using the Docker image [paulczar/omgwtfssl](https://github.com/paulczar/omgwtfssl).

Requirements
------------

Docker installed and prepared.

Role Variables
--------------

| Variable | Example | Description |
|----------|---------|-------------|
| work_dir | /home/my-name/.ssl | Directory where the file will be generated. Defaults to "{{ ansible_env.HOME }}/ssl". |
| ssl_dns  | node1.example.org |  Full qualified hostname. |
| ca_subject  | certificate-for-node1 | Certificate subject text. |

Dependencies
------------

None.

Example Playbook
----------------

```plain
- hosts: localhost
  gather_facts: true
  connection: local

  roles:
    - role: ansible-ssl-cert-docker
      vars:
        ssl_dns: "node1.example.org"
        ca_subject: "Certificate issued for domain: node1.example.org"
```

License
-------

BSD

Author Information
------------------

fwalk___gitlab

